//
//  CBMainVC + KolodaView.swift
//  Crowdbotics
//
//  Created by Vitalii Hudenko on 12/2/17.
//  Copyright © 2017 Vitalii Hudenko. All rights reserved.
//

import Foundation
import Koloda
import UIKit

extension CBMainVC:KolodaViewDelegate, KolodaViewDataSource{
    
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return developers.count
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .fast
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        guard let devView = CBDeveloperCardView.loadView() else{
            return UIView()
        }
        
        let model = developers[index]
        
        devView.imgAvatar.image = model.gender == .male ? UIImage(named: "avatar_male") : UIImage(named: "avatar_female")
        devView.lblName.text = model.name
        devView.lblSkill.text = model.skill
        return devView
    }
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        koloda.resetCurrentCardIndex()
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        let item = developers[index]
        switch direction {
        case .left, .topLeft, .bottomLeft:
            declinedDevelopers.update(with: item)
            acceptedDevelopers.remove(item)
            break
        case .right, .topRight, .bottomRight:
            acceptedDevelopers.update(with: item)
            declinedDevelopers.remove(item)
            break
        default:
            break
        }
    }
    
    
    func koloda(_ koloda: KolodaView, allowedDirectionsForIndex index: Int) -> [SwipeResultDirection] {
        return [SwipeResultDirection.left, .right, .topLeft, .topRight, .bottomLeft, .bottomRight]
    }
}
