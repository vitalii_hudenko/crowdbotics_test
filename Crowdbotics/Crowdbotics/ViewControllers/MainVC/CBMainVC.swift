//
//  CBMainVC.swift
//  Crowdbotics
//
//  Created by Vitalii Hudenko on 12/2/17.
//  Copyright © 2017 Vitalii Hudenko. All rights reserved.
//

import UIKit
import Koloda
import KRProgressHUD

class CBMainVC: UIViewController {

    
    
    @IBOutlet weak var collectionView: KolodaView!{
        didSet{
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    
    var developers = [CBDeveloper]()
    var acceptedDevelopers = Set<CBDeveloper>()
    var declinedDevelopers = Set<CBDeveloper>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        parseDevelopers()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CBDevelopersVC,
            let devs = sender as? [CBDeveloper] {
            vc.developers = devs
        }
    }
    
    //MARK:----
    //MARK:Actions
    @IBAction func btnAcceptAction(_ sender: Any) {
        collectionView.swipe(.bottomRight, force: true)
    }
    
    @IBAction func btnDeclineAction(_ sender: Any) {
        collectionView.swipe(.bottomLeft, force: true)
    }
    
    @IBAction func btnResetAction(_ sender: Any) {
        acceptedDevelopers = []
        declinedDevelopers = []
        collectionView.resetCurrentCardIndex()
    }
    
    @IBAction func btnDeclinedAction(_ sender: Any) {
        let developers = declinedDevelopers.sorted(by: {$0.name < $1.name})
        performSegue(withIdentifier: "Developers", sender: developers)
    }
    
    @IBAction func btnAcceptedAction(_ sender: Any) {
        let developers = acceptedDevelopers.sorted(by: {$0.name < $1.name})
        performSegue(withIdentifier: "Developers", sender: developers)
    }
    
    //MARK:----
    //MARK:Instance
    fileprivate func parseDevelopers(){
        KRProgressHUD.show()
        CBDataParser.parseDevelopersData({
            [weak self](devs) in
            KRProgressHUD.dismiss()
            self?.developers = devs
            self?.acceptedDevelopers = []
            self?.declinedDevelopers = []
            self?.collectionView.reloadData()
        }) { [weak self](error) in
            KRProgressHUD.dismiss()
            self?.presentError(error)
        }
    }
    
}
