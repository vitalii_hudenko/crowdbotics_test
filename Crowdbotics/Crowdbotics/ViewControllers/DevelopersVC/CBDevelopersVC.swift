//
//  CBDevelopersVC.swift
//  Crowdbotics
//
//  Created by Vitalii Hudenko on 12/2/17.
//  Copyright © 2017 Vitalii Hudenko. All rights reserved.
//

import UIKit

class CBDevelopersVC: UITableViewController {

    var developers = [CBDeveloper]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:----
    //MARK:UITableViewDataSource/Delegate
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return developers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeveloperCell", for: indexPath)
        let item = developers[indexPath.row]
        
        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = item.skill
        cell.imageView?.image = item.gender == .male ? UIImage(named: "avatar_male") : UIImage(named: "avatar_female")
        
        return cell
    }

}
