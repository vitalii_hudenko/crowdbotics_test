//
//  Localized.swift
//  Crowdbotics
//
//  Created by Vitalii Hudenko on 12/2/17.
//  Copyright © 2017 Vitalii Hudenko. All rights reserved.
//

import Foundation

protocol Localized {
    var localizedDescription:String{get}
}
