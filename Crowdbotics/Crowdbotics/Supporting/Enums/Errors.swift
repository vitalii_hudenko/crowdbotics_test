//
//  Errors.swift
//  Crowdbotics
//
//  Created by Vitalii Hudenko on 12/2/17.
//  Copyright © 2017 Vitalii Hudenko. All rights reserved.
//

import Foundation
import ObjectMapper

enum CBError:Error {
    case unexpected, dataNotFound, dataParsingError
}

extension CBError:Localized{
    var localizedDescription: String{
        switch self {
        case .unexpected:
            return NSLocalizedString("Unexpected error",
                                     tableName: "Errors",
                                     comment: "Unexpected error")
        case .dataNotFound:
            return NSLocalizedString("Developers data not found",
                                     tableName: "Errors",
                                     comment: "Error message when developer data not found")
        case .dataParsingError:
            return NSLocalizedString("Developer data can't be parsed",
                                     tableName: "Errors",
                                     comment: "Error message when developer data can't be parsed")
        }
    }
}

