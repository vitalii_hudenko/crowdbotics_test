//
//  CBDeveloperCardView.swift
//  Crowdbotics
//
//  Created by Vitalii Hudenko on 12/2/17.
//  Copyright © 2017 Vitalii Hudenko. All rights reserved.
//

import UIKit

class CBDeveloperCardView: UIView {

    class func loadView()->CBDeveloperCardView?{
        let xib = Bundle.main.loadNibNamed("CBDeveloperCardView", owner: self, options: [:])
        return xib?.last as? CBDeveloperCardView
    }
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSkill: UILabel!
    
    
}
