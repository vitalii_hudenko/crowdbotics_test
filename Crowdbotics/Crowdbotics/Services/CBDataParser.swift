//
//  CBDataParser.swift
//  Crowdbotics
//
//  Created by Vitalii Hudenko on 12/2/17.
//  Copyright © 2017 Vitalii Hudenko. All rights reserved.
//

import Foundation
import ObjectMapper

typealias ParseCallback<T:Any> = (T)->()

class CBDataParser{
    class func parseDevelopersData(_ onSuccess:@escaping ParseCallback<[CBDeveloper]>, onFail:@escaping ParseCallback<Error>){
        guard let dataUrl = Bundle.main.url(forResource: Constants.devDataFileName, withExtension: "json") else{
            onFail(CBError.dataNotFound)
            return
        }
        DispatchQueue.global().async {
            do{
                let data = try Data(contentsOf: dataUrl)
                guard let json = try JSONSerialization.jsonObject(with: data, options:[]) as? [[String:Any]] else{
                    DispatchQueue.main.async {
                        onFail(CBError.dataParsingError)
                    }
                    return
                }
                let developers = try json.map({ (item) -> Map in
                    return Map(mappingType: .fromJSON, JSON: item)
                })
                    .map({ (map) -> CBDeveloper in
                        guard let model = CBDeveloper(map: map) else{
                            throw CBError.unexpected
                        }
                        model.mapping(map: map)
                        return model
                    })
                DispatchQueue.main.async {
                    onSuccess(developers)
                }
            }
            catch{
                DispatchQueue.main.async {
                    onFail(error)
                }
            }
        }
    }
}
