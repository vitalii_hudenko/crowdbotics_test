//
//  CBDeveloper.swift
//  Crowdbotics
//
//  Created by Vitalii Hudenko on 12/2/17.
//  Copyright © 2017 Vitalii Hudenko. All rights reserved.
//

import Foundation
import ObjectMapper

class CBDeveloper:Mappable{
    
    fileprivate let uid = UUID()
    
    fileprivate(set) var name:String!
    fileprivate(set) var skill:String!
    fileprivate(set) var gender:Gender!
    
    //MARK:----
    //MARK:Mappable
    required init?(map: Map) {}
    func mapping(map: Map) {
        name <- map[MapKey.name]
        skill <- map[MapKey.skill]
        gender <- map[MapKey.sex]
    }
    
}

extension CBDeveloper:Hashable{
    var hashValue: Int {
        return uid.hashValue
    }
    
    static func ==(lhs: CBDeveloper, rhs: CBDeveloper) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}


fileprivate enum MapKey:String, Key{
    case name, skill, sex
}
