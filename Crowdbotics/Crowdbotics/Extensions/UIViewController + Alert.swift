//
//  UIViewController + Alert.swift
//  Crowdbotics
//
//  Created by Vitalii Hudenko on 12/2/17.
//  Copyright © 2017 Vitalii Hudenko. All rights reserved.
//

import Foundation
import UIKit

typealias ActionHandler = (UIAlertAction)->()

extension UIViewController{
    func presentError(_ error:Error, okAction:ActionHandler? = nil){
        var message = ""
        if let error = error as? Localized{
            message = error.localizedDescription
        }
        else{
            message = error.localizedDescription
        }
        let title = NSLocalizedString("Ooops",
                                    tableName: "Errors",
                                    comment: "Error alert view title")
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let titleOk = NSLocalizedString("Ok",
                                        tableName: "General",
                                        comment: "Ok button title")
        let ok = UIAlertAction(title: titleOk, style: .default, handler: okAction)
        alert.addAction(ok)
        present (alert, animated: true, completion: nil)
    }
}
