//
//  ObjectMapper + Mapping.swift
//  Crowdbotics
//
//  Created by Vitalii Hudenko on 12/2/17.
//  Copyright © 2017 Vitalii Hudenko. All rights reserved.
//

import Foundation
import ObjectMapper


extension Map{
    subscript<T:Key>(key:T)->Map{
        return self[key.rawValue, delimiter: ".", ignoreNil: false]
    }
}
